import 'package:flutter/material.dart';
import 'package:mtn/featurescreens.dart';
import 'package:mtn/main.dart';

void main3() {
  runApp(const MaterialApp(
    title: 'Login',

    home: DashboardRoute(),
  ));
}

class DashboardRoute extends StatelessWidget {
  const DashboardRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(primary: Colors.orange[500], textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        backgroundColor: Colors.orange,
        actions: [TextButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => const ProfileRoute()),);
          },
          child: Text('Edit Profile', style: TextStyle(color: Colors.white,)),
          ),
        ],
      ),
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.start,
         children: [ 
           const SizedBox(height: 20),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                    ListTile(
                    leading: Icon(Icons.arrow_right_outlined, color: Colors.blue),
                    title: Text('Create new article'),
                    subtitle: Text('publish your work to everyone'),
                  ),
                ],
              ),
           ),
           const SizedBox(height: 10),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    ListTile(
                    leading: const Icon(Icons.arrow_right_outlined, color: Colors.blue),
                    title: const Text('Articles feed'),
                    subtitle: const Text('3 new unread posts', style: TextStyle(color: Colors.red),),
                    onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => const ArticlesRoute()),);
                    },
                  ),

                ],
              ),
           ),
           const SizedBox(height: 10),
           Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    ListTile(
                    leading: const Icon(Icons.arrow_right_outlined, color: Colors.blue),
                    title: const Text('Message inbox'),
                    subtitle: const Text('0 new messages',),
                    onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => const InboxRoute()),);
                    },
                  ),

                ],
              ),
           ),
         ],                                                                   
       )

    );
  }
}






class ProfileRoute extends StatelessWidget {
  const ProfileRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(primary: Colors.orange[500], textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        backgroundColor: Colors.orange[500],
      ),
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.start,
         children: [ 
           const SizedBox(height: 10),
           Stack(
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 120,
                  child:            CircleAvatar(  
             radius: 160.0,      
             child: ClipRRect(
               child: Image.asset('assets/a.png', fit: BoxFit.fill,), 
               borderRadius: BorderRadius.circular(50.0),
             ),             
           ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Icon(Icons.add, size: 40,),
                )
              ],
           ),

           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Change username',
           ),
           ),
           ),
           const SizedBox(height: 10),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Update email',
           ),
           ),
           ),
           const SizedBox(height: 10),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Update cell number',
           ),
           ),
           ),
           const SizedBox(height: 30),
           ElevatedButton(
              style: style,
                     onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const DashboardRoute()),);
                     },
                     child: const Text('Done'),
           ),

         ],                                                                   

       )

    );
  }
}
