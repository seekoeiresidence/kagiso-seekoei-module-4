import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/material.dart';
import 'package:mtn/dashboard.dart';
import 'package:mtn/featurescreens.dart';


void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Module 4',
    home: SplashScreen(),
    
    theme: ThemeData(
      primaryColor: Colors.orange[500],
      fontFamily: 'Calibri',
       scaffoldBackgroundColor: Colors.white,
       primarySwatch: Colors.orange,
       
       textTheme: TextTheme(bodyText1: TextStyle(color: Colors.black12)),
    ),
    
  ));
}


class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Container(
                height: 100,
                width: 100,
                child: Image.asset('assets/b.jpg'), 
                ),
        nextScreen: FirstRoute(),
        splashTransition: SplashTransition.rotationTransition,
        pageTransitionType: PageTransitionType.fade,

    );
  }
}



class FirstRoute extends StatelessWidget {
  const FirstRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(primary: Colors.orange[500], textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login'),
        backgroundColor: Colors.orange[500],
      ),
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.center,
         children: [ 
           Container(        
             height: 100,
             width: 100,
             child: Image.asset('assets/b.jpg'),
           ),

           const SizedBox(height: 30),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter username',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter Password',
           ),
           ),
           ),
           const SizedBox(height: 50),
           ElevatedButton(
              style: style,
                     onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const DashboardRoute()),);
                     },
                     child: const Text('login'),
           ),

           const SizedBox(height: 30),
           
           TextButton(
             style: TextButton.styleFrom(
               textStyle: const TextStyle(fontSize:15, color: Colors.blue),
             ),
                onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const SecondRoute()),);
                },
             child: const Text('New here? Register'),
           ),
           
          

         ],                                                                   
         


       )

    );
  }
}

class SecondRoute extends StatelessWidget {
  const SecondRoute({super.key});

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(primary: Colors.orange[500], textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
        backgroundColor: Colors.orange[500],
      ),

/********************* */
body: Column(
crossAxisAlignment: CrossAxisAlignment.center,
         mainAxisAlignment: MainAxisAlignment.center,
         children: [ 
           Row(children: [Text('Register',style: TextStyle(fontSize: 40, color: Colors.blue,),)],),

           const SizedBox(height: 30),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter username',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter email',
           ),
           ),
           ),
           const SizedBox(height: 20),
           Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
           child: TextFormField(   
           decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter password',
           ),
           ),
           ),           
           const SizedBox(height: 50),
           ElevatedButton(
              style: style,
                     onPressed: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const FirstRoute()),);
                     },
                     child: const Text('Register'),
           ),

           const SizedBox(height: 30),
           
           TextButton(
             style: TextButton.styleFrom(
               textStyle: const TextStyle(fontSize:15, color: Colors.blue),
             ),
                onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const FirstRoute()),);
                },
             child: const Text('Already registered? Login'),
           ),
           
          

         ],                                                                   
         


       )

/********************* */

      );
  }
}




